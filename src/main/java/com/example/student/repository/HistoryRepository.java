package com.example.student.repository;

import com.example.student.entity.History;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.UUID;

public interface HistoryRepository extends JpaRepository<History, UUID> {

    Page<History> findAllByStudent_Id(UUID id, Pageable pageable);

    Page<History> findAllByCreatedAtBetweenOrderByCreatedAtDesc(LocalDateTime beginDate, LocalDateTime endDate, Pageable pageable);

    Page<History> findAllByStudent_IdAndCreatedAtBetweenOrderByCreatedAtDesc(UUID id, LocalDateTime beginDate, LocalDateTime endDate, Pageable pageable);
}
