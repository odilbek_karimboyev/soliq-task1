package com.example.student.controller;

import com.example.student.exception.HistoryNotFoundException;
import com.example.student.exception.StudentNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@RequiredArgsConstructor
@Slf4j
public class ControllerAdvice {

    @ExceptionHandler(StudentNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    StudentNotFoundException send(StudentNotFoundException e){
        log.error(e.getMessage());
        return e;
    }

    @ExceptionHandler(HistoryNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    HistoryNotFoundException send(HistoryNotFoundException e){
        log.error(e.getMessage());
        return e;
    }
}
