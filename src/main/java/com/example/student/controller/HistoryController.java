package com.example.student.controller;

import com.example.student.payload.HistoryRequestDto;
import com.example.student.payload.HistoryResponseDto;
import com.example.student.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.UUID;

@RequiredArgsConstructor
@RequestMapping("/history")
@RestController
public class HistoryController {

    private final HistoryService historyService;

    @GetMapping
    public Page<HistoryResponseDto> getAll(Pageable pageable) {
        return historyService.getAll(pageable);
    }

    @GetMapping("/filter/id")
    public Page<HistoryResponseDto> getAllByStudentId(@RequestParam UUID id, Pageable pageable) {
        return historyService.getAllByStudentId(id, pageable);
    }

    @GetMapping("/filter/between")
    public Page<HistoryResponseDto> getAllBetween(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate beginDate,
                                                  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate endDate,
                                                  Pageable pageable) {
        return historyService.getAllBetween(beginDate, endDate, pageable);
    }

    @GetMapping("/filter/one/between")
    public Page<HistoryResponseDto> getOneStudentBetween(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate beginDate,
                                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate endDate,
                                                         @RequestParam UUID id, Pageable pageable) {
        return historyService.getOneStudentBetween(id, beginDate, endDate, pageable);
    }

    @PostMapping
    public UUID create(@RequestBody HistoryRequestDto request) {
        return historyService.create(request);
    }

}
