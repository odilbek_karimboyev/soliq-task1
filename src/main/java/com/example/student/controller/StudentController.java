package com.example.student.controller;

import com.example.student.payload.StudentRequestDto;
import com.example.student.payload.StudentResponseDto;
import com.example.student.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public Page<StudentResponseDto> getAll(Pageable pageable) {
        return studentService.getAll(pageable);
    }

    @GetMapping("/filter")
    public StudentResponseDto getById(@RequestParam UUID id) {
        return studentService.getById(id);
    }

    @PostMapping
    public UUID create(@RequestBody StudentRequestDto request) {
        return studentService.creat(request);
    }

    @PutMapping
    public StudentResponseDto update(@RequestParam UUID id, @RequestBody StudentRequestDto request) {
        return studentService.update(id, request);
    }

    @DeleteMapping
    public void delete(@RequestParam UUID id) {
        studentService.delete(id);
    }

}
