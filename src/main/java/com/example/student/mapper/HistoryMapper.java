package com.example.student.mapper;

import com.example.student.entity.History;
import com.example.student.payload.HistoryRequestDto;
import com.example.student.payload.HistoryResponseDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@DecoratedWith(HistoryMapperDecorator.class)
public interface HistoryMapper {

    @Mapping(source = "createdAt", target = "time")
    HistoryResponseDto toDto(History history);

    @Mapping(target = "student",ignore = true)
    History fromDto(HistoryRequestDto request);

}
