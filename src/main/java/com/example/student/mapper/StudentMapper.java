package com.example.student.mapper;

import com.example.student.entity.Student;
import com.example.student.payload.StudentRequestDto;
import com.example.student.payload.StudentResponseDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface StudentMapper {

    StudentResponseDto toDto(Student student);

    Student fromDto(StudentRequestDto request);

}
