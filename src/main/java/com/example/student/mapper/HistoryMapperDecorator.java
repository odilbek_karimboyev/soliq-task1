package com.example.student.mapper;

import com.example.student.entity.History;
import com.example.student.entity.Student;
import com.example.student.exception.StudentNotFoundException;
import com.example.student.payload.HistoryRequestDto;
import com.example.student.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Optional;

public abstract class HistoryMapperDecorator implements HistoryMapper {

    @Autowired
    @Qualifier("delegate")
    private HistoryMapper mapper;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public History fromDto(HistoryRequestDto request) {
        History history = mapper.fromDto(request);
        Optional<Student> byId = studentRepository.findById(request.getStudentId());
        if (byId.isEmpty()) throw new StudentNotFoundException("student not found");
        history.setStudent(byId.get());
        return history;
    }
}
