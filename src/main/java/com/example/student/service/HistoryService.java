package com.example.student.service;

import com.example.student.payload.HistoryRequestDto;
import com.example.student.payload.HistoryResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.UUID;

public interface HistoryService {

    Page<HistoryResponseDto> getAll(Pageable pageable);

    Page<HistoryResponseDto> getAllByStudentId(UUID id, Pageable pageable);

    Page<HistoryResponseDto> getAllBetween(LocalDate beginDate, LocalDate endDate, Pageable pageable);

    Page<HistoryResponseDto> getOneStudentBetween(UUID id, LocalDate beginDate, LocalDate endDate, Pageable pageable);

    UUID create(HistoryRequestDto request);

}
