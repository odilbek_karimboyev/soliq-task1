package com.example.student.service.impl;

import com.example.student.entity.History;
import com.example.student.mapper.HistoryMapper;
import com.example.student.payload.HistoryRequestDto;
import com.example.student.payload.HistoryResponseDto;
import com.example.student.repository.HistoryRepository;
import com.example.student.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class HistoryServiceImpl implements HistoryService {

    private final HistoryRepository historyRepository;
    private final HistoryMapper historyMapper;

    @Override
    public Page<HistoryResponseDto> getAll(Pageable pageable) {
        Page<History> all = historyRepository.findAll(pageable);
        return all.map(historyMapper::toDto);
    }

    @Override
    public Page<HistoryResponseDto> getAllByStudentId(UUID id, Pageable pageable) {
        Page<History> allByStudent_id = historyRepository.findAllByStudent_Id(id, pageable);
        return allByStudent_id.map(historyMapper::toDto);
    }

    @Override
    public Page<HistoryResponseDto> getAllBetween(LocalDate beginDate, LocalDate endDate, Pageable pageable) {
        LocalDateTime dayStart = beginDate.atStartOfDay().with(LocalTime.MIN);
        LocalDateTime dayEnd = endDate.atStartOfDay().with(LocalTime.MAX);
        Page<History> allByCreatedAtBetweenOrderByCreatedAtDesc = historyRepository.findAllByCreatedAtBetweenOrderByCreatedAtDesc(dayStart, dayEnd, pageable);
        return allByCreatedAtBetweenOrderByCreatedAtDesc.map(historyMapper::toDto);
    }

    @Override
    public Page<HistoryResponseDto> getOneStudentBetween(UUID id, LocalDate beginDate, LocalDate endDate, Pageable pageable) {
        LocalDateTime dayStart = beginDate.atStartOfDay().with(LocalTime.MIN);
        LocalDateTime dayEnd = endDate.atStartOfDay().with(LocalTime.MAX);
        Page<History> allByCreatedAtBetweenOrderByCreatedAtDesc = historyRepository.findAllByStudent_IdAndCreatedAtBetweenOrderByCreatedAtDesc(id, dayStart, dayEnd, pageable);
        return allByCreatedAtBetweenOrderByCreatedAtDesc.map(historyMapper::toDto);
    }

    @Override
    public UUID create(HistoryRequestDto request) {
        History save = historyRepository.save(historyMapper.fromDto(request));
        return save.getId();
    }
}
