package com.example.student.service.impl;

import com.example.student.entity.Student;
import com.example.student.exception.StudentNotFoundException;
import com.example.student.mapper.StudentMapper;
import com.example.student.payload.StudentRequestDto;
import com.example.student.payload.StudentResponseDto;
import com.example.student.repository.StudentRepository;
import com.example.student.service.StudentService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    private final ObjectMapper objectMapper;

    @Override
    public Page<StudentResponseDto> getAll(Pageable pageable) {
        Page<Student> all = studentRepository.findAll(pageable);
        return all.map(studentMapper::toDto);
    }

    @Override
    public StudentResponseDto getById(UUID id) {
        Optional<Student> byId = studentRepository.findById(id);
        if (byId.isPresent())
            return studentMapper.toDto(byId.get());
        else throw new StudentNotFoundException("student not found");
    }

    @Override
    public UUID creat(StudentRequestDto request) {
        Student save = studentRepository.save(studentMapper.fromDto(request));
        return save.getId();
    }

    @Override
    public StudentResponseDto update(UUID id, StudentRequestDto request) {
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        Optional<Student> byId = studentRepository.findById(id);
        if (byId.isEmpty()) throw new StudentNotFoundException("student not found");

        Student student;
        try {
            student = objectMapper.updateValue(byId.get(), request);
            return studentMapper.toDto(studentRepository.save(student));
        } catch (JsonMappingException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public void delete(UUID id) {
        studentRepository.deleteById(id);
    }
}
