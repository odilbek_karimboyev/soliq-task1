package com.example.student.service;

import com.example.student.payload.StudentRequestDto;
import com.example.student.payload.StudentResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface StudentService {

    Page<StudentResponseDto> getAll(Pageable pageable);

    StudentResponseDto getById(UUID id);

    UUID creat(StudentRequestDto request);

    StudentResponseDto update(UUID id,StudentRequestDto request);

    void delete(UUID id);
}
