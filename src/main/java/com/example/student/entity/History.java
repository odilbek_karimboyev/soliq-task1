package com.example.student.entity;

import com.example.student.enums.Action;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class History extends BaseDomain {

    @ManyToOne
    private Student student;

    @Enumerated(EnumType.STRING)
    private Action action;

}
