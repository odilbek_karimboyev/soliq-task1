package com.example.student.entity;

import lombok.*;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Student extends BaseDomain {

    private String firstName;

    private String lastName;

    private int age;

}
