package com.example.student.payload;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@Data
public class StudentRequestDto {
    private String firstName;
    private String lastName;
    private int age;
}
