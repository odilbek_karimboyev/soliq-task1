package com.example.student.payload;

import com.example.student.enums.Action;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@Data
public class HistoryRequestDto {

    private UUID studentId;

    private Action action;
}
