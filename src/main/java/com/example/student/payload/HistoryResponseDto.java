package com.example.student.payload;

import com.example.student.entity.Student;
import com.example.student.enums.Action;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Data
public class HistoryResponseDto {

    private Student student;

    private Action action;

    private LocalDateTime time;
}
